import javax.swing.JTextArea;
import javax.swing.SwingWorker;


public class PrimeWorker extends SwingWorker<Boolean, Object>{

	int value = 0;
	JTextArea jTextArea;
	static int count = 0;
	
	PrimeWorker(int _value, JTextArea _jTextArea) {
		value = _value;
		jTextArea = _jTextArea;
	}
	
	@Override
	protected Boolean doInBackground() throws Exception {
		for (int i = 2; i <= Math.sqrt(value); i++) {
			if 
			(value%i==0) {
				return false;
			}
		}
		synchronized(jTextArea) {
			count++;
			jTextArea.insert(count + ":" + value + "\n", 0);
		}
		return true;
	}
}
