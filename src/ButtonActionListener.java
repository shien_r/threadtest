import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;

public class ButtonActionListener implements ActionListener {

	JTextArea jTextArea;
	
	ButtonActionListener(JTextArea _jTextArea) {
		jTextArea = _jTextArea;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		PrimeWorker.count = 0;
		jTextArea.setText("");
		for (int i = 1; PrimeWorker.count < 100 && i < 541; i++) {
			PrimeWorker primeWorker = new PrimeWorker(i,jTextArea);
			primeWorker.execute();
		}		
	}

}
